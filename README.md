# Wordpress Big File Upload

Updated the Tuxedo Big File Uploads plugin to display its menu in the /settings/media/ menu. 
Note: The max upload limit is set by: php.ini, then Wordpress multisite parameter. The above plugin allows to override the php.ini limit, but not the multis